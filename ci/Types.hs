{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Types
  ( RunResult(..)
  , TestedPatch(..)
  , PackageResult(..)
  , BuildInfo(..)
  , BuildResult(..)
  ) where

import Cabal.Plan
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Aeson
import qualified Data.Text as T
import GHC.Generics
import Cabal.Plan

-- | Information about a unit which we attempted to build.
data BuildInfo
  = BuildInfo { pkgName :: PkgName
              , version :: Ver
              , flags :: M.Map FlagName Bool
              , dependencies :: S.Set UnitId
              }
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | The result of a unit build.
data BuildResult
  = BuildSucceeded { buildLog :: T.Text }
    -- ^ the build succeeded.
  | BuildPreexisted
    -- ^ the unit pre-existed in the global package database.
  | BuildFailed { buildLog :: T.Text }
    -- ^ the build failed
  | BuildNotAttempted
    -- ^ the build was not attempted either because a dependency failed or it
    -- is an executable or testsuite component
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | The result of an attempt to tested a patch
data PackageResult
  = PackagePlanningFailed { planningError :: T.Text }
    -- ^ Our attempt to build the package resulting in no viable install plan.
  | PackageResult { packageBuilt :: Bool
                  , units :: M.Map UnitId (BuildInfo, BuildResult)
                  }
    -- ^ We attempted to build the package.
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | Information about a patch which we tested.
data TestedPatch
  = TestedPatch { patchedPackageName :: PkgName
                , patchedPackageVersion :: Ver
                , patchedPackageResult :: PackageResult
                }
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)

-- | The result of a CI run.
data RunResult
  = RunResult { testedPatches :: [TestedPatch] }
  deriving stock (Show, Generic)
  deriving anyclass (ToJSON, FromJSON)
