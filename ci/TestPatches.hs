{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module TestPatches
  ( testPatches
  , Config(..), config
  ) where

import Control.Monad
import Data.Foldable
import Data.List (intercalate)
import Data.Maybe
import Data.Text (Text)
import GHC.Generics
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Encoding.Error as TE
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import Data.Aeson
import qualified Data.Map.Strict as M
import qualified Data.Map.Merge.Strict as M
import qualified Data.Set as S

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Text.PrettyPrint.ANSI.Leijen as PP
import Text.PrettyPrint.ANSI.Leijen (Doc, vcat, (<+>))
import System.FilePath
import System.Directory
import System.Environment (getEnvironment)
import System.Exit
import System.Process.Typed
import System.IO.Temp
import System.IO
import Cabal.Plan
import NeatInterpolation
import Options.Applicative

import Types
import MakeConstraints
import Utils

data Config = Config { configPatchDir :: FilePath
                     , configCompiler :: FilePath
                     , configGhcOptions :: [String]
                     , configCabalOptions :: [String]
                     , configOnlyPackages :: Maybe (S.Set Cabal.PackageName)
                     , configConcurrency :: Int
                     , configExtraCabalFragments :: [FilePath]
                     }

cabalOptions :: Config -> [String]
cabalOptions cfg =
  configCabalOptions cfg ++
  [ "-w", configCompiler cfg
  ] ++ concatMap (\opt -> ["--ghc-option", opt]) (configGhcOptions cfg)

config :: Parser TestPatches.Config
config =
  TestPatches.Config
    <$> patchDir
    <*> compiler
    <*> ghcOptions
    <*> cabalOptions
    <*> onlyPackages
    <*> concurrency
    <*> extraCabalFragments
  where
    patchDir = option str (short 'p' <> long "patches" <> help "patch directory" <> value "./patches")
    compiler = option str (short 'w' <> long "with-compiler" <> help "path of compiler")
    ghcOptions = many $ option str (short 'f' <> long "ghc-option" <> help "flag to pass to compiler")
    cabalOptions = many $ option str (short 'F' <> long "cabal-option" <> help "flag to pass to cabal-install")
    onlyPackages =
      fmap (Just . S.fromList) (some $ option pkgName (short 'o' <> long "only" <> help "filter packages"))
      <|> pure Nothing
    concurrency = option auto (short 'j' <> long "concurrency" <> value 1 <> help "number of concurrent builds")
    extraCabalFragments = many $ option str (long "extra-cabal-fragment" <> help "path of extra configuration to include in cabal project files")

    pkgName :: ReadM Cabal.PackageName
    pkgName = str >>= maybe (fail "invalid package name") pure . simpleParse

testPatches :: Config -> IO ()
testPatches cfg = do
  setup cfg
  packages <- findPatchedPackages (configPatchDir cfg)
  let packages' :: S.Set (Cabal.PackageName, Version)
      packages'
        | Just only <- configOnlyPackages cfg
        = S.fromList $ filter (\(pname,_) -> pname `S.member` only) packages
        | otherwise
        = S.fromList packages

  let build :: (Cabal.PackageName, Version) -> IO [TestedPatch]
      build (pname, ver) = do
        res <- buildPackage cfg pname ver
        let tpatch = TestedPatch { patchedPackageName = PkgName $ T.pack $ display pname
                                 , patchedPackageVersion = Ver $ versionNumbers ver
                                 , patchedPackageResult = res
                                 }
        return [tpatch]
  testedPatches <- fold <$> mapConcurrentlyN (fromIntegral $ configConcurrency cfg) build (S.toList packages')
  let runResult = RunResult testedPatches

  print $ resultSummary runResult
  BSL.writeFile "results.json" $ encode runResult
  exitWith $ if anyFailures runResult then ExitFailure 1 else ExitSuccess

anyFailures :: RunResult -> Bool
anyFailures (RunResult testedPatches) =
    any patchFailed testedPatches
  where
    patchFailed :: TestedPatch -> Bool
    patchFailed tp =
      case patchedPackageResult tp of
        PackagePlanningFailed _ -> True
        PackageResult False _ -> True
        _ -> False

resultSummary :: RunResult -> Doc
resultSummary runResult = vcat
  [ "Total units built:" <+> pshow (length allUnits)
  , ""
  , pshow (length planningErrors) <+> "had no valid install plan:"
  , PP.indent 4 $ vcat $ map (uncurry prettyPkgVer) planningErrors
  , ""
  , pshow (length failedUnits) <+> "units failed to build:"
  , PP.indent 4 $ vcat [ prettyPkgVer (pkgName binfo) (version binfo)
                     | (binfo, _) <- M.elems failedUnits
                     ]
  , ""
  , pshow (length failedDependsUnits) <+> "units failed to build due to unbuildable dependencies."
  ]
  where
    planningErrors :: [(PkgName, Ver)]
    planningErrors =
      [ (patchedPackageName tpatch, patchedPackageVersion tpatch)
      | tpatch <- testedPatches runResult
      , PackagePlanningFailed _ <- pure $ patchedPackageResult tpatch
      ]

    allUnits :: M.Map UnitId (BuildInfo, BuildResult)
    allUnits = M.unions
      [ units
      | tpatch <- testedPatches runResult
      , PackageResult _ units <- pure $ patchedPackageResult tpatch
      ]

    failedUnits :: M.Map UnitId (BuildInfo, BuildResult)
    failedUnits = M.filter failed allUnits
      where failed (_, BuildFailed _) = True
            failed _ = False

    failedDependsUnits :: M.Map UnitId (S.Set UnitId)
    failedDependsUnits = M.filter (not . S.null) (failedDeps allUnits)

toPkgName :: Cabal.PackageName -> PkgName
toPkgName = PkgName . T.pack . display

toVer :: Version -> Ver
toVer = Ver . versionNumbers

-- | For @cabal-plan@ types.
prettyPkgVer :: PkgName -> Ver -> Doc
prettyPkgVer (PkgName pname) (Ver ver) =
  PP.blue (PP.text $ T.unpack pname) <+> PP.green (PP.text $ intercalate "." $ map show ver)

-- | For @Cabal@ types.
prettyPackageVersion :: Cabal.PackageName -> Version -> Doc
prettyPackageVersion pname version =
  prettyPkgVer (toPkgName pname) (toVer version)

buildPackage :: Config -> Cabal.PackageName -> Version -> IO PackageResult
buildPackage cfg pname version = do
  logMsg $ "=> Building" <+> prettyPackageVersion pname version
  compilerId <- getCompilerId (configCompiler cfg)

  -- prepare the test package
  createDirectoryIfMissing True dirName
  copyFile "cabal.project" (dirName </> "cabal.project")
  copyFile "cabal.project.local" (dirName </> "cabal.project.local")
  appendFile (dirName </> "cabal.project") "packages: .\n"
  TIO.writeFile
    (dirName </> concat ["test-", display pname, ".cabal"])
    (makeTestCabalFile pname version)

  -- run the build
  code <- runProcess $ setWorkingDir dirName
                     $ proc "cabal"
                     $ ["new-build"] ++ cabalOptions cfg

  -- figure out what happened
  let planPath = dirName </> "dist-newstyle" </> "cache" </> "plan.json"
  planExists <- doesFileExist planPath
  case planExists of
    True -> do
      Just plan <- decode <$> BSL.readFile planPath :: IO (Maybe PlanJson)
      cabalDir <- getCabalDirectory
      let logDir = cabalDir </> "logs" </> compilerId
      results <- mapM (checkUnit logDir) (pjUnits plan)
      logMsg $
        let result = case code of
              ExitSuccess -> PP.cyan "succeeded"
              ExitFailure n -> PP.red "failed" <+> PP.parens ("code" <+> pshow n)
        in "=> Build of" <+> prettyPackageVersion pname version <+> result
      return $ PackageResult (code == ExitSuccess) (mergeInfoPlan (planToBuildInfo plan) results)
    False -> do
      logMsg $ PP.red $ "=> Planning for" <+> prettyPackageVersion pname version <+> "failed"
      return $ PackagePlanningFailed mempty
  where
    planToBuildInfo :: PlanJson -> M.Map UnitId BuildInfo
    planToBuildInfo plan = M.fromList
      [ (uId unit, info)
      | unit <- M.elems $ pjUnits plan
      , let depends :: S.Set UnitId
            depends = fold
              [ ciLibDeps comp <> ciExeDeps comp
              | comp <- M.elems $ uComps unit
              ]
      , let PkgId pname pvers = uPId unit
      , let info = BuildInfo { pkgName = pname
                             , version = pvers
                             , flags = uFlags unit
                             , dependencies = depends
                             }
      ]

    checkUnit :: FilePath -> Unit -> IO BuildResult
    checkUnit logDir unit
      | UnitTypeBuiltin <- uType unit = return BuildPreexisted
      | UnitTypeLocal <- uType unit = return $ BuildSucceeded "inplace"
      | otherwise = do
      exists <- doesFileExist logPath
      case exists of
        True -> do
          buildLog <- TE.decodeUtf8With TE.lenientDecode <$> BS.readFile logPath
          if | T.null buildLog
               -> return $ BuildFailed buildLog
             | any isInstallingLine $ take 5 $ reverse $ T.lines buildLog
               -> return $ BuildSucceeded buildLog
             | otherwise
               -> return $ BuildFailed buildLog
        False -> return BuildNotAttempted
      where
        isInstallingLine line = "Installing" `T.isPrefixOf` line
        logPath =
          case uId unit of
            UnitId uid -> logDir </> T.unpack uid <.> "log"

    mergeInfoPlan :: Ord k
                  => M.Map k BuildInfo
                  -> M.Map k BuildResult
                  -> M.Map k (BuildInfo, BuildResult)
    mergeInfoPlan = M.merge err err (M.zipWithMatched $ \_ x y -> (x,y))
      where
        err = M.mapMissing $ \_ _ -> error "error merging"

    dirName = "test-" ++ display pname ++ "-" ++ display version

makeTestCabalFile :: Cabal.PackageName -> Version -> T.Text
makeTestCabalFile pname' ver' =
  [text|
    cabal-version:       2.2
    name: test-$pname
    version: 1.0

    library
      exposed-modules:
      build-depends: $pname == $ver
      default-language: Haskell2010
  |]
  where
    pname = T.pack $ display pname'
    ver = T.pack $ display ver'

setup :: Config -> IO ()
setup cfg = do
  keysExist <- doesDirectoryExist "keys"
  unless keysExist $ do
    cabalDir <- getCabalDirectory
    -- Work around cabal-install bug; it seems to get confused by repository changes
    removePathForcibly $ cabalDir </> "packages" </> repoName
    createDirectoryIfMissing True $ cabalDir </> "packages" </> repoName
    runProcess_ $ proc "build-repo.sh" ["gen-keys"]

  cwd <- getCurrentDirectory
  environ <- getEnvironment
  let env = environ ++
            [ ("REPO_NAME", repoName)
            , ("REPO_URL", "file://" ++ (cwd </> "repo"))
            , ("PATCHES", configPatchDir cfg)
            ]

  removePathForcibly "cabal.project"
  removePathForcibly "cabal.project.local"
  runProcess_
    $ setEnv env
    $ proc "build-repo.sh" ["build-repo"]

  projectFile <- openFile "cabal.project" WriteMode
  runProcess_
    $ setStdout (useHandleClose projectFile)
    $ setEnv env
    $ proc "build-repo.sh" ["build-repository-blurb"]

  projectLocalFile <- openFile "cabal.project.local" WriteMode
  runProcess_
    $ setStdout (useHandleClose projectLocalFile)
    $ setEnv env
    $ proc "build-repo.sh" ["build-constraints"]

  extraFragments <- mapM readFile (configExtraCabalFragments cfg)
  constraints <- makeConstraints (configPatchDir cfg)
  appendFile "cabal.project" $ show $ vcat $
    [ constraints
    , "with-compiler: " <> PP.text (configCompiler cfg)
    ] ++ map PP.text extraFragments

  runProcess_ $ proc "cabal" ["new-update"]

  -- Force cabal to rebuild the index cache.
  buildPackage cfg "acme-box" (mkVersion [0,0,0,0])
  return ()
  where
    repoName = "local"

-- | Compute for each unit which of its dependencies failed to build.
failedDeps :: M.Map UnitId (BuildInfo, BuildResult) -> M.Map UnitId (S.Set UnitId)
failedDeps pkgs =
  let res = fmap f pkgs -- N.B. Knot-tied

      f :: (BuildInfo, BuildResult) -> S.Set UnitId
      f (binfo, result) =
        failedDirectDeps <> failedTransDeps
        where
          failedTransDeps = S.unions $ map (res M.!) (S.toList $ dependencies binfo)
          failedDirectDeps = S.filter failed (dependencies binfo)

          failed :: UnitId -> Bool
          failed uid =
            case snd $ pkgs M.! uid of
              BuildFailed _ -> True
              _ -> False
  in res
