{ pkgs }:

let
  # Maps Haskell package names to a list of their native library dependency
  # attributes.
  pkgDeps= with pkgs;
    {
      zlib = [ zlib ];
      digest = [ zlib ];
    };

  mkCabalFragment = pkgName: deps:
    with pkgs.lib;
    let
      libDirs = concatStringsSep " " (map (dep: getOutput "lib" dep + "/lib") deps);
      includeDirs = concatStringsSep " " (map (dep: getOutput "dev" dep + "/include") deps);
    in ''
    package ${pkgName}
      extra-lib-dirs: ${libDirs}
      extra-include-dirs: ${includeDirs}
    '';
in
  pkgs.lib.concatStringsSep "\n" (pkgs.lib.mapAttrsToList mkCabalFragment pkgDeps)
