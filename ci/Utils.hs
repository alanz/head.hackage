module Utils where

import Control.Monad

import qualified Distribution.Package as Cabal
import Distribution.Text
import Distribution.Types.Version hiding (showVersion)

import qualified Data.ByteString.Lazy.Char8 as BSL

import System.Directory
import System.FilePath
import System.Process.Typed

import Control.Exception (bracket_)
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Concurrent.STM.TSem

import Text.PrettyPrint.ANSI.Leijen (Doc)
import qualified Text.PrettyPrint.ANSI.Leijen as PP

parsePatchName :: FilePath -> Maybe (Cabal.PackageName, Version)
parsePatchName fname = do
  pid <- simpleParse (takeBaseName fname) :: Maybe Cabal.PackageId
  let pname = Cabal.packageName pid
      ver = Cabal.packageVersion pid
  guard $ not $ null $ versionNumbers ver
  return (pname, ver)

findPatchedPackages :: FilePath  -- ^ patch directory
                    -> IO [(Cabal.PackageName, Version)]
findPatchedPackages patchDir = do
  patchFiles <- listDirectory patchDir
  return [ (pname, ver)
         | fname <- patchFiles
         , let err = error $ "Invalid patch file name: " ++ fname
         , (pname, ver) <- maybe err pure $ parsePatchName fname
         ]

getCompilerId :: FilePath -> IO String
getCompilerId ghcPath = do
  (_, out) <- readProcessStdout $ proc ghcPath ["--info"]
  let parsed = read $ BSL.unpack out :: [(String, String)]
  case lookup "Project version" parsed of
    Just val -> return ("ghc-" ++ val)
    Nothing -> fail "error fetching compiler id"

getCabalDirectory :: IO FilePath
getCabalDirectory = getAppUserDataDirectory "cabal"

mapConcurrentlyN :: Integer -> (a -> IO b) -> [a] -> IO [b]
mapConcurrentlyN n f xs = do
  sem <- atomically $ newTSem n
  let g x = bracket_ (atomically $ waitTSem sem) (atomically $ signalTSem sem) (f x)
  xs' <- mapM (async . g) xs
  mapM wait xs'

logMsg :: Doc -> IO ()
logMsg msg = print msg

pshow :: Show a => a -> Doc
pshow = PP.text . show

prettyVersion :: Version -> Doc
prettyVersion =
  PP.hcat . PP.punctuate (PP.text ".") . map pshow . versionNumbers

prettyPackageName :: Cabal.PackageName -> Doc
prettyPackageName =
  PP.text . Cabal.unPackageName
