{ nixpkgs ? (import (import ./base-nixpkgs.nix) {}) }:

with nixpkgs;
let
  hackage-repo-tool =
    let
      src = fetchFromGitHub {
        owner = "haskell";
        repo = "hackage-security";
        rev = "474768743f407edef988d4153f081b2c662fe84f";
        sha256 = "1n46ablyna72zl9whirfv72l715wlmi6jahbf02asbsxkx3b7xnm";
      };
    in haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {};

  overlay-tool =
    let
      src = fetchFromGitHub {
        owner = "bgamari";
        repo = "hackage-overlay-repo-tool";
        rev = "18eb61c830ad908d36d343f400a1588af6b9a03a";
        sha256 = "1y1fw5x9lyd533lm67s7iyzb4640y8lya11sdjia0yd1j5if6s40";
      };
    in haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src {};

  head-hackage-ci =
    haskellPackages.callCabal2nix "head-hackage-ci" ./. {};

  buildDeps = import ./build-deps.nix { pkgs = nixpkgs; };

  buildDepsFile = pkgs.writeText "deps.cabal.project" buildDeps;

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        cabal-install ghc gcc binutils-unwrapped pwgen gnused
        hackage-repo-tool overlay-tool python3
      ];
    in
      runCommand "repo" {
        nativeBuildInputs = [ makeWrapper ];
        cabalDepsSrc = buildDeps;
      } ''
        mkdir -p $out/bin
        makeWrapper ${head-hackage-ci}/bin/head-hackage-ci $out/bin/head-hackage-ci \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${xz}/bin/xz $out/bin/xz
      '';
in
  build-repo
